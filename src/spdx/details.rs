use std::fmt::Display;

use color_print::{cformat, cstr};
use http_cache_reqwest::{CACacheManager, Cache, CacheMode, HttpCache, HttpCacheOptions};
use reqwest::Client;
use reqwest_middleware::ClientBuilder;
use serde::Deserialize;

use crate::{
    consts::{EMAIL, OWNER, REPO, YEAR},
    util::{cache::http_cache_dir, errors::LictoolResult},
};

#[derive(Debug, Deserialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct LicenseDetails {
    pub is_deprecated_license_id: bool,
    pub license_text: String,
    pub name: String,
    pub license_comments: Option<String>,
    pub license_id: String,
    pub see_also: Vec<String>,
    pub is_osi_approved: bool,
    pub is_fsf_libre: Option<bool>,
    pub deprecated_version: Option<String>,
}

impl Display for LicenseDetails {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut output = String::new();
        let term_width = termion::terminal_size().unwrap().0 as usize;
        let padding = (term_width - self.name.len()) / 2;
        write!(f, "{:width$}", "", width = padding)?;
        writeln!(f, "{}", cformat!("«<s>{}</>»", self.name))?;
        output.push_str(&cformat!(
            "<s>Reference:</> <u>https://spdx.org/licenses/{}.html</>\n",
            self.license_id
        ));
        output.push_str(&cformat!("<s>License ID:</> {}\n", self.license_id));
        if let Some(ref comments) = self.license_comments {
            output.push_str(&cformat!("<s>License Comments:</> {}\n", comments));
        }
        output.push_str(&cformat!("<s>See Also:</>\n"));
        for link in &self.see_also {
            output.push_str(&cformat!("  - <u>{}</>\n", link));
        }
        output.push_str(&cformat!(
            "<s>Is Supported License ID:</> {}\n",
            (!self.is_deprecated_license_id).as_checkbox()
        ));
        output.push_str(&cformat!(
            "<s>Is OSI Approved:</> {}",
            self.is_osi_approved.as_checkbox()
        ));
        if let Some(is_fsf_libre) = self.is_fsf_libre {
            output.push_str(&cformat!(
                "\n<s>Is FSF Free/Libre:</> {}",
                is_fsf_libre.as_checkbox()
            ));
        }
        if let Some(ref deprecated_version) = self.deprecated_version {
            output.push_str(&cformat!(
                "\n<s>Deprecated Version:</> {}",
                deprecated_version
            ));
        }
        write!(f, "{}", output)
    }
}

impl LicenseDetails {
    pub fn contains_year(&self) -> bool {
        YEAR.iter().any(|&word| self.license_text.contains(word))
    }

    pub fn contains_owner(&self) -> bool {
        OWNER.iter().any(|&word| self.license_text.contains(word))
    }

    pub fn contains_repo(&self) -> bool {
        REPO.iter().any(|&word| self.license_text.contains(word))
    }

    pub fn contains_email(&self) -> bool {
        EMAIL.iter().any(|&word| self.license_text.contains(word))
    }
}

trait Checkbox {
    fn as_checkbox(&self) -> &str;
}

impl Checkbox for bool {
    fn as_checkbox(&self) -> &str {
        match self {
            true => cstr!("<green, bold>󰄲</>"),
            false => cstr!("<red, bold>󰅗</>"),
        }
    }
}

pub async fn fetch_license_details(details_url: &str) -> LictoolResult<LicenseDetails> {
    let client = ClientBuilder::new(Client::new())
        .with(Cache(HttpCache {
            mode: CacheMode::Default,
            manager: CACacheManager {
                path: http_cache_dir(),
            },
            options: HttpCacheOptions::default(),
        }))
        .build();
    let res = client
        .get(details_url)
        .send()
        .await?
        .json::<LicenseDetails>()
        .await?;
    Ok(res)
}
