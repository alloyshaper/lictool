use std::fmt::Display;

use color_print::cformat;
use http_cache_reqwest::{CACacheManager, Cache, CacheMode, HttpCache, HttpCacheOptions};
use reqwest::Client;
use reqwest_middleware::ClientBuilder;
use serde::Deserialize;
extern crate reqwest;
use crate::util::{cache::http_cache_dir, errors::LictoolResult};
pub mod details;
pub use details::LicenseDetails;

use self::details::fetch_license_details;

pub const SPDX_LICENSES: &str = "https://spdx.org/licenses/licenses.json";

#[derive(Debug, Deserialize, Clone)]
pub struct Licenses {
    #[serde(rename = "licenses")]
    pub body: Vec<License>,
}

impl Licenses {
    pub async fn new() -> LictoolResult<Self> {
        fetch_licenses().await
    }

    pub fn filter_by(
        &self,
        deprecated: bool,
        supported: bool,
        osi_approved: bool,
        fsf_libre: bool,
    ) -> Vec<&License> {
        self.body
            .iter()
            .filter(|license| {
                let mut result = true;
                if deprecated {
                    result = result && license.is_deprecated_license_id == deprecated;
                }
                if supported {
                    result = result && license.is_deprecated_license_id != supported;
                }
                if osi_approved {
                    result = result && license.is_osi_approved == osi_approved;
                }
                if fsf_libre {
                    result = result && license.is_fsf_libre == Some(fsf_libre);
                }
                result
            })
            .collect()
    }
}

async fn fetch_licenses() -> LictoolResult<Licenses> {
    let client = ClientBuilder::new(Client::new())
        .with(Cache(HttpCache {
            mode: CacheMode::Default,
            manager: CACacheManager {
                path: http_cache_dir(),
            },
            options: HttpCacheOptions::default(),
        }))
        .build();
    let res = client
        .get(SPDX_LICENSES)
        .send()
        .await?
        .json::<Licenses>()
        .await?;
    Ok(res)
}

pub fn display_license_ids(licenses: &mut [&License]) -> LictoolResult<()> {
    licenses.sort_by_key(|license| license.is_deprecated_license_id);
    licenses
        .iter()
        .for_each(|license| println!("{}", license.color_id()));
    Ok(())
}

#[derive(Debug, Deserialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct License {
    // pub reference: String,
    pub is_deprecated_license_id: bool,
    pub details_url: String,
    // pub reference_number: i64,
    // pub name: String,
    #[serde(rename = "licenseId")]
    pub id: String,
    // pub see_also: Vec<String>,
    pub is_osi_approved: bool,
    pub is_fsf_libre: Option<bool>,
}

impl License {
    pub async fn details(&self) -> LictoolResult<LicenseDetails> {
        fetch_license_details(&self.details_url).await
    }
    pub fn color_id(&self) -> String {
        if self.is_deprecated_license_id {
            cformat!("<bold, red>{}</>", self.id)
        } else {
            cformat!("<bold, green>{}</>", self.id)
        }
    }
}

impl Display for License {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.id)
    }
}
