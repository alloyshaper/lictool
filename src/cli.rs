use clap::CommandFactory;
use clap::{Parser, Subcommand};
use clap_complete::{generate, Shell};
use dialoguer::theme::ColorfulTheme;
use dialoguer::FuzzySelect;
use std::io;

use crate::spdx::{display_license_ids, Licenses};
use crate::template::{fill_license_forms, write_template, Template};
use crate::util::errors::{Error, LictoolResult};

#[derive(Parser, Debug)]
#[command(author, version,styles=get_styles())]
#[clap(arg_required_else_help = true)]
pub struct Cli {
    #[clap(subcommand)]
    subcommand: CliCommand,
}

impl Cli {
    /// Handles the received command.
    pub async fn handle(&self) -> LictoolResult<()> {
        match &self.subcommand {
            CliCommand::Completions { shell } => {
                generate(
                    shell.to_owned(),
                    &mut Cli::command(),
                    env!("CARGO_BIN_NAME"),
                    &mut io::stdout().lock(),
                );
                Ok(())
            }
            CliCommand::Init { path } => {
                let licenses = Licenses::new().await?;
                let selection = FuzzySelect::with_theme(&ColorfulTheme::default())
                    .with_prompt("Select a license")
                    .items(&licenses.body)
                    .max_length(7)
                    .interact_opt()?;
                let license = &licenses.body[selection.unwrap_or(0)];
                let mut details = license.details().await?;
                let mut template = fill_license_forms(&mut details, &ColorfulTheme::default())?;
                Ok(write_template(path, &mut template)?)
            }
            CliCommand::List {
                deprecated,
                supported,
                osi_approved,
                fsf_libre,
            } => {
                let licenses = Licenses::new().await?;
                let mut filtered =
                    licenses.filter_by(*deprecated, *supported, *osi_approved, *fsf_libre);
                display_license_ids(&mut filtered)
            }
            CliCommand::Add {
                license_id,
                owner,
                email,
                repo,
                year,
                path,
            } => {
                let licenses = Licenses::new().await?;
                if let Some(license) = licenses
                    .body
                    .iter()
                    .find(|lic| lic.to_string() == *license_id)
                {
                    let details = license.details().await?;
                    Ok(write_template(
                        path,
                        &mut Template {
                            license_text: details.license_text,
                            year: *year,
                            owner: owner.clone(),
                            repo: repo.clone(),
                            email: email.clone(),
                        },
                    )?)
                } else {
                    Err(Error::NotFound)?
                }
            }
            CliCommand::Info { license_id } => {
                let licenses = Licenses::new().await?;
                if let Some(license) = licenses
                    .body
                    .iter()
                    .find(|lic| lic.to_string() == *license_id)
                {
                    let details = license.details().await?;
                    println!("{}", details);
                } else {
                    Err(Error::NotFound)?
                }
                Ok(())
            }
        }
    }
}

#[derive(Subcommand, Debug)]
/// Available commands
enum CliCommand {
    /// Initializes a license, prompting for details to fill placeholders
    Init {
        #[clap(short, long)]
        #[clap(default_value_t = String::from("LICENSE.md"))]
        path: String,
    },
    /// Add a license in the current directory without prompting for individual details
    Add {
        license_id: String,
        #[arg(short, long, alias = "author")]
        owner: Option<String>,
        #[arg(short, long)]
        email: Option<String>,
        #[arg(short, long)]
        repo: Option<String>,
        #[arg(short, long)]
        year: Option<i32>,
        #[clap(short, long)]
        #[clap(default_value_t = String::from("LICENSE.md"))]
        path: String,
    },
    /// Lists all available licenses
    List {
        /// Only deprecated
        #[arg(short, long)]
        deprecated: bool,
        #[arg(short, long)]
        /// Only supported
        supported: bool,
        #[arg(short, long)]
        /// Only OSI Approved
        osi_approved: bool,
        #[arg(short, long)]
        /// Only FSF Free/Libre
        fsf_libre: bool,
    },
    /// Get info about license
    Info { license_id: String },
    /// Generate completion scripts for your shell
    Completions {
        #[clap(value_enum)]
        shell: Shell,
    },
}

/// Returns the styles that will be applied to the CLI.
fn get_styles() -> clap::builder::Styles {
    clap::builder::Styles::styled()
        .usage(
            anstyle::Style::new()
                .bold()
                .underline()
                .fg_color(Some(anstyle::Color::Rgb(anstyle::RgbColor(196, 153, 243)))),
        )
        .header(
            anstyle::Style::new()
                .bold()
                .underline()
                .fg_color(Some(anstyle::Color::Rgb(anstyle::RgbColor(255, 121, 198)))),
        )
        .literal(
            anstyle::Style::new()
                .fg_color(Some(anstyle::Color::Rgb(anstyle::RgbColor(80, 250, 123)))),
        )
        .invalid(
            anstyle::Style::new()
                .bold()
                .fg_color(Some(anstyle::Color::Rgb(anstyle::RgbColor(255, 85, 85)))),
        )
        .error(
            anstyle::Style::new()
                .bold()
                .fg_color(Some(anstyle::Color::Rgb(anstyle::RgbColor(255, 85, 85)))),
        )
        .valid(
            anstyle::Style::new()
                .bold()
                .underline()
                .fg_color(Some(anstyle::Color::Rgb(anstyle::RgbColor(80, 250, 123)))),
        )
        .placeholder(
            anstyle::Style::new()
                .fg_color(Some(anstyle::Color::Rgb(anstyle::RgbColor(241, 250, 140)))),
        )
}
