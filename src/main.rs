use clap::Parser;
use cli::Cli;
use util::errors::{display_error, LictoolResult};

mod cli;
mod consts;
mod spdx;
mod template;
mod util;

#[tokio::main]
async fn main() -> LictoolResult<()> {
    let args = Cli::parse();
    if let Err(e) = args.handle().await {
        display_error(&e);
        std::process::exit(1);
    }
    Ok(())
}
