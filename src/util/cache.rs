use std::path::PathBuf;

pub(crate) fn http_cache_dir() -> PathBuf {
    dirs::cache_dir()
        .expect("Cache directory not found.")
        .join(env!("CARGO_PKG_NAME"))
        .join("http-cache")
}
