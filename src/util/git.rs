use std::process::Command;

pub(crate) fn git_username() -> Option<String> {
    let output = Command::new("git")
        .arg("config")
        .arg("--global")
        .arg("--get")
        .arg("user.name")
        .output()
        .ok()?;

    if output.status.success() {
        let username = String::from_utf8(output.stdout).ok()?;
        Some(username)
    } else {
        None
    }
}

pub(crate) fn git_email() -> Option<String> {
    let output = Command::new("git")
        .arg("config")
        .arg("--global")
        .arg("--get")
        .arg("user.email")
        .output()
        .ok()?;

    if output.status.success() {
        let email = String::from_utf8(output.stdout).ok()?;
        Some(email)
    } else {
        None
    }
}
