use color_print::cformat;
use snafu::prelude::*;
pub type LictoolResult<T> = anyhow::Result<T>;

#[derive(Snafu, Debug)]
pub(crate) enum Error {
    #[snafu(display("No license found matching the ID provided."))]
    NotFound,
}

pub(crate) fn display_error(err: &anyhow::Error) {
    eprintln!("{}", cformat!("<rgb(255, 85, 85), bold>error:</> {}", err));
    for cause in err.chain().skip(1) {
        eprintln!("{}", cformat!("\n<rgb(255, 121, 198), bold>Caused by:</>"));
        for line in cause.to_string().lines() {
            if line.is_empty() {
                eprintln!();
            } else {
                eprintln!("{}", cformat!("   <rgb(255, 184, 108)>{}</>", line))
            }
        }
    }
}
