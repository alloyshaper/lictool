use std::{fmt::Display, fs, io, mem::take, path::Path};

use crate::{
    spdx::LicenseDetails,
    util::git::{git_email, git_username},
};
use chrono::{Datelike, Local};
use color_print::cprintln;
use dialoguer::{
    theme::{ColorfulTheme, Theme},
    Input,
};

use crate::{
    consts::{EMAIL, OWNER, REPO, YEAR},
    util::errors::LictoolResult,
};

#[derive(Debug, Default)]
pub struct Template {
    pub license_text: String,
    pub year: Option<i32>,
    pub owner: Option<String>,
    pub repo: Option<String>,
    pub email: Option<String>,
}

impl Template {
    fn render(&mut self) -> String {
        let mut res = take(&mut self.license_text);
        if let Some(year) = self.year {
            YEAR.iter()
                .for_each(|&word| res = res.replace(word, &year.to_string()));
        }
        if let Some(owner) = &self.owner {
            OWNER
                .iter()
                .for_each(|&word| res = res.replace(word, owner));
        }
        if let Some(repo) = &self.repo {
            REPO.iter().for_each(|&word| res = res.replace(word, repo));
        }

        if let Some(email) = &self.email {
            EMAIL
                .iter()
                .for_each(|&word| res = res.replace(word, email));
        }
        res
    }
}

pub fn fill_license_forms(
    details: &mut LicenseDetails,
    theme: &dyn Theme,
) -> LictoolResult<Template> {
    let mut template = Template::default();
    if details.contains_owner() {
        let owner: String = Input::with_theme(theme)
            .with_prompt("Please enter the author's name")
            .show_default(true)
            .default(git_username().unwrap_or_default())
            .interact_text()
            .unwrap();
        template.owner = Some(owner);
    }
    if details.contains_year() {
        let year: i32 = Input::with_theme(theme)
            .with_prompt("Please enter the year of creation")
            .show_default(true)
            .default(Local::now().year())
            .interact_text()
            .unwrap();
        template.year = if year == 0 { None } else { Some(year) };
    }
    if details.contains_repo() {
        let repo: String = Input::with_theme(theme)
            .with_prompt("Please enter the program's name")
            .allow_empty(true)
            .interact_text()
            .unwrap();
        template.repo = if repo.is_empty() { None } else { Some(repo) };
    }
    if details.contains_email() {
        let email: String = Input::with_theme(theme)
            .with_prompt("Please enter the email")
            .default(git_email().unwrap_or_default())
            .allow_empty(true)
            .interact_text()
            .unwrap();
        template.email = if email.is_empty() { None } else { Some(email) };
    }
    template.license_text = take(&mut details.license_text);
    Ok(template)
}

pub fn write_template<P: AsRef<Path> + Display>(
    path: P,
    template: &mut Template,
) -> Result<(), io::Error> {
    let result = match path.as_ref().exists() && path.as_ref().is_file() {
        true => {
            cprintln!(
                "<y, bold>\u{f421}</> <bold>The {} file already exists.</>",
                path
            );
            let path: String = Input::with_theme(&ColorfulTheme::default())
                .with_prompt("Please specify a new file name to avoid overwriting.")
                .default(path.to_string())
                .interact_text()
                .unwrap();
            write_template(path.as_str(), template)
        }
        false => {
            fs::write(&path, template.render())?;
            cprintln!("<green>✔</> <bold>Successfully created {} file.</>", path);
            Ok(())
        }
    };
    result
}
